/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    All rights reserved.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: main.h
        Version:   v0.0
        Date:      2019-10-11
    ============================================================================
 */

#ifndef MAIN_H
#define MAIN_H

#include <stdint.h>


#define RF95_HEADER_LEN 4
#define MAC_HEADER_SIZE 3
#define DATA_LEN 33

#define BUFFER_SIZE 32
#define BUFFER_DATA_SIZE (RF95_HEADER_LEN+MAC_HEADER_SIZE+DATA_LEN)

#define RSSICORR 164

#define SPICSN LATBbits.LATB7
#define RESET LATAbits.LATA4 
#define LED_A LATAbits.LATA2 

#define TRUE 1
#define FALSE 0

typedef struct slme {
    
    uint8_t radio_initialized;
    uint8_t sender_initialized;
    uint8_t receiver_initialized;
    
    int16_t pkt_snr;
    uint8_t pkt_rssi;
    uint8_t rssi;
    
}slme;

typedef struct {
  unsigned volatile int read_pointer;
  unsigned volatile int write_pointer;
  unsigned volatile int data_size;
  uint8_t buffer[BUFFER_SIZE][BUFFER_DATA_SIZE];
} rBufPar;

extern slme SLME;
extern uint8_t printAllReg, tx_test;
extern rBufPar txBuf, rxBuf;

#endif /* MAIN_H */