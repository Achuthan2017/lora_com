/*  ============================================================================
Copyright (C) 2015 Achuthan Paramanathan.
================================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
================================================================================
Revision Information:
File name: zeus.c
Version:   v0.0
Date:      31-10-2019
================================================================================
*/


/*
** =============================================================================
**                        INCLUDE STATEMENTS
** =============================================================================
*/
#include <stdio.h>

#include "xc.h"
#include "main.h"
#include "sx1278.h"
#include "timerdriver.h"
#include "usbdriver.h"
#include "sx1278_register.h"
#include "ringBuffer.h"

/*
** =============================================================================
**                           LOCAL DEFINES
** =============================================================================
*/


/*
** =============================================================================
**                   LOCAL FUNCTION DECLARATIONS
** =============================================================================
*/
void resetRadio(void);
void setModeSleep(void);
void setModeIdle(void);
void setModeTx(void);
void setModeLoRa(void);
uint8_t readRegister(uint8_t addr);
void writeRegister(uint8_t addr, uint8_t value);
void writeBurstRegister(uint8_t adr, uint8_t* data, uint8_t len);
uint8_t receivePkt(uint8_t *payload);
void setModeRx(void);

/*
** =============================================================================
**                       LOCAL VARIABLES
** =============================================================================
*/

static uint8_t sendcnt = 0;
static uint8_t tx_done_irq = 0;



// The Frequency Synthesizer step = RH_RF95_FXOSC / 2^^19
#define RH_RF95_FSTEP  (RH_RF95_FXOSC / 524288)

// The crystal oscillator frequency of the module
#define RH_RF95_FXOSC 32000000.0

// The length of the header.
// The headers are inside the LORA's payload


/*
** =============================================================================
**                       IMPLEMENTATION
** =============================================================================
*/

void waitPacketSent( void )
{
    while(!tx_done_irq);
}


/*==============================================================================
** Function...: receivepacket
** Return.....: void
** Description: Receive the packet.
** Created....: 1.03.2020 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void receivepacket(void)
{
    long int SNR;
    uint8_t payload[BUFFER_DATA_SIZE];

    if (! buffer_full(&rxBuf))
    {
        uint8_t value = readRegister(RF95_REG_19_PKT_SNR_VALUE);
        if( value & 0x80 ) // The SNR sign bit is 1
        {
            // Invert and divide by 4
            value = ( ( ~value + 1 ) & 0xFF ) >> 2;
            SNR = -value;
        }
        else
        {
            // Divide by 4
            SNR = ( value & 0xFF ) >> 2;
        }

        int irqflags = readRegister(RF95_REG_12_IRQ_FLAGS);
        // clear radio IRQ 
        writeRegister(RF95_REG_12_IRQ_FLAGS, RF95_RX_DONE);

        //  payload crc: 0x20
        if((irqflags & RF95_PAYLOAD_CRC_ERROR) == RF95_PAYLOAD_CRC_ERROR)
        {
            usb_print("CRC error\n\r");
            // clear flag
            writeRegister(RF95_REG_12_IRQ_FLAGS, RF95_PAYLOAD_CRC_ERROR);
        } else 
        {
            uint8_t currentAddr = readRegister(RF95_REG_10_FIFO_RX_CURRENT_ADDR);
            uint8_t receivedCount = readRegister(RF95_REG_13_RX_NB_BYTES);

            writeRegister(RF95_REG_0D_FIFO_ADDR_PTR, currentAddr);

/* 
 * ----------------------------------------------------------------------
 * | RSSI | PRSSI | PSIZE | ADD1 | ADD2 | ADD3 | DATASIZE | PCNT | DATA |
 * |--------------------------------------------------------------------|
 * |  1   |   1   |   1   |  1   |  1   |  1   |    1     |   1  | 1-36 |
 * |--------------------------------------------------------------------|  
 */ 
            
            int i = 0;
            int16_t rssi = 0;
            int16_t prssi = 0;
            
            rssi = readRegister(0x1B);
            prssi = readRegister(0x1A);   
            
            memset(payload, 0, BUFFER_DATA_SIZE);
            
            payload[0] = rssi;
            payload[1] = prssi;
            payload[2] = receivedCount;

            for( i=0; i < receivedCount; i++)
            {
                payload[i+MAC_HEADER_SIZE] = (char)readRegister(RF95_REG_00_FIFO);
            }
            
            write_buffer(payload, &rxBuf);
            
            LED_A ^= 1;
    }
    
    } // if any buffer free
}

/*==============================================================================
** Function...: radioIrqHandler
** Return.....: void
** Description: Handles the incoming IRQ. Called from ext int.
** Created....: 1.03.2020 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void radioIrqHandler( void )
{
    uint8_t irqflags = readRegister(RF95_REG_12_IRQ_FLAGS);
    
    if( irqflags &  RF95_RX_DONE )
    {
        receivepacket();
    }
    else if ( irqflags &  RF95_TX_DONE )
    {
        tx_done_irq = 1;
    }
}



/*==============================================================================
** Function...: initiateReceiver
** Return.....: uint8_t
** Description: Initiate the receiver
** Created....: 29.02.2020 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
uint8_t initiateReceiver(void)
{
    if(!SLME.radio_initialized)
    {
        usb_print("ERROR! initiate receiver\n\r");
        SLME.receiver_initialized = FALSE;
        return 1;
    }
    
    //setModeIdle();
    
    // configure DIO mapping DIO0=RxDone DIO1=RxTout DIO2=NOP
    writeRegister(RF95_REG_40_DIO_MAPPING1, RF95_MAP_DIO0_LORA_RXDONE|
                                            RF95_MAP_DIO1_LORA_RXTOUT|
                                            RF95_MAP_DIO2_LORA_NOP);
    // clear all radio IRQ flags
    writeRegister(RF95_REG_12_IRQ_FLAGS, 0xFF);
    // mask all IRQs but RxDone
    writeRegister(RF95_REG_11_IRQ_FLAGS_MASK, ~RF95_RX_DONE_MASK);
    
    setModeRx();
    SLME.receiver_initialized = TRUE;
    
    return 0;
}

/*==============================================================================
** Function...: initiateSender
** Return.....: uint8_t
** Description: Initiate the Sender
** Created....: 29.02.2020 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
uint8_t initiateSender(void)
{
    if(!SLME.radio_initialized)
    {
        usb_print("ERROR! initiate sender\n\r");
        SLME.sender_initialized = FALSE;
        return 1;
    }
    
    //setModeIdle();
    
    // set the IRQ mapping DIO0=TxDone DIO1=NOP DIO2=NOP
    writeRegister(RF95_REG_40_DIO_MAPPING1, RF95_TX_DONE|
                                            RF95_MAP_DIO1_LORA_NOP|
                                            RF95_MAP_DIO2_LORA_NOP); 
     
    // clear all radio IRQ flags
    writeRegister(RF95_REG_12_IRQ_FLAGS, 0xFF);
    // mask all IRQs but TxDone
    writeRegister(RF95_REG_11_IRQ_FLAGS_MASK, ~RF95_TX_DONE_MASK);
     
    
    SLME.sender_initialized = TRUE;
    
    return 0;
}

/*==============================================================================
** Function...: send
** Return.....: uint8_t
** Description: Send a packet using sx1278
** Created....: 10.02.2020 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
uint8_t send(uint8_t* data, uint8_t len)
{
    if(!SLME.sender_initialized)
    {
        usb_print("ERROR! send\n\r");
        return 1;
    }
    
    
    writeRegister(RF95_REG_0E_FIFO_TX_BASE_ADDR, 0x00);
    writeRegister(RF95_REG_0D_FIFO_ADDR_PTR, 0x00);
    
    sendcnt = 1 + sendcnt;
    // The headers
    writeRegister(RF95_REG_00_FIFO, 2);
    writeRegister(RF95_REG_00_FIFO, 2);
    writeRegister(RF95_REG_00_FIFO, 2);
    writeRegister(RF95_REG_00_FIFO, sendcnt);
    
  
    // The message data
    writeBurstRegister(RF95_REG_00_FIFO, data, len);
    writeRegister(RF95_REG_22_PAYLOAD_LENGTH, len + RF95_HEADER_LEN);
    
    setModeTx();
    waitPacketSent(); 
    
    LED_A ^= 1;
    
    return 0;
}


/*==============================================================================
** Function...: setupLora
** Return.....: uint8_t
** Description: Setup sx1278 for lora
** Created....: 10.02.2020 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
uint8_t setupLora(void)
{
    resetRadio();
    
    setModeSleep();
    setModeLoRa();
    
    delay_ms(10);
        
    if( readRegister(RF95_REG_01_OP_MODE) != ( RF95_MODE_SLEEP | 
                                               RF95_LONG_RANGE_MODE | 
                                               RF95_MODE_LF )  )
    {
        usb_print("ERROR! not in sleep mode\n\r");
        SLME.radio_initialized = FALSE;
    }
    
    // Set up FIFO
    // Configure the entire 256 bytes of the FIFO for both receive and transmit,
    // not both at the same time
    writeRegister(RF95_REG_0E_FIFO_TX_BASE_ADDR, 0);
    writeRegister(RF95_REG_0F_FIFO_RX_BASE_ADDR, 0);
    
    setModeIdle();
    delay_ms(10);
    
    // Using  Explicit Header mode, CR 4/5, bw 125 kHz
    writeRegister(RF95_REG_1D_MODEM_CONFIG1,       0x72);
    // CRC Enabled, single packet send mode, SF 7
    writeRegister(RF95_REG_1E_MODEM_CONFIG2,       0x74);
    //  LNA gain set by the internal AGC loop, LowDataRateOptimize disabled
    writeRegister(RF95_REG_26_MODEM_CONFIG3,       0x04);
    
    //setPreambleLength,  Default is 8
    writeRegister(RF95_REG_20_PREAMBLE_MSB, 8 >> 8);
    writeRegister(RF95_REG_21_PREAMBLE_LSB, 8 & 0xff);
    
    // set frequency
    uint32_t frf = (433.1 * 1000000.0) / RH_RF95_FSTEP;
    writeRegister(RF95_REG_06_FRF_MSB, (uint8_t)(frf>>16) );
    writeRegister(RF95_REG_07_FRF_MID, (uint8_t)(frf>> 8) );
    writeRegister(RF95_REG_08_FRF_LSB, (uint8_t)(frf>> 0) );
    
    //set power to 14
    writeRegister(RF95_REG_09_PA_CONFIG, RF95_PA_SELECT | (14));
    writeRegister(LORA_REG_SYNCWORD, LORA_MAC_PREAMBLE);
    

    writeRegister(RF95_REG_0C_LNA, RF95_LNA_MAX_GAIN);  // max lna gain
    // set max payload size
    writeRegister(RF95_REG_23_MAX_PAYLOAD_LENGTH, 64);
    
    SLME.radio_initialized = TRUE;
    
    return 0;
}



/*==============================================================================
** Function...: printAllRegisters
** Return.....: void
** Description: For debugging print out all the regs
** Created....: 31.10.2019 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void printAllRegisters(void)
{
    uint8_t value = 0x0;
    uint8_t i = 0;
    char s[20];
    
    for(i=0; i<69; i++)
    {
        value = readRegister(i);
        sprintf(s,"0x%02X 0x%02X\n\r", i, value);
        usb_print(s);
        delay_ms(100);
    }
}


/*==============================================================================
** Function...: resetRadio
** Return.....: void
** Description: Reset the radio
** Created....: 31.10.2019 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void resetRadio(void)
{
    RESET = 1;
    delay_ms(150);
    RESET = 0;
    delay_ms(150);
    RESET = 1;
    delay_ms(150);
}


/*==============================================================================
** Function...: readRegister
** Return.....: uint8_t
** Description: Read the value from an address
** Created....: 31.10.2019 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
uint8_t readRegister(uint8_t addr)
{
    uint8_t spibuf[2];
    spibuf[0] = addr & 0x7F;
    spibuf[1] = 0x00;
    SPICSN = 0;
    SPI1_Exchange8bitBuffer(spibuf , 2,  spibuf);
    SPICSN = 1;
    return spibuf[1];
}


/*==============================================================================
** Function...: writeRegister
** Return.....: void
** Description: Write a value to an address
** Created....: 31.10.2019 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void writeRegister(uint8_t addr, uint8_t value)
{
    uint8_t spibuf[2];
    spibuf[0] = addr | 0x80;
    spibuf[1] = value;
    SPICSN = 0;
    SPI1_Exchange8bitBuffer(spibuf , 2,  spibuf);
    SPICSN = 1;
}


/*==============================================================================
** Function...: writeBurstRegister
** Return.....: void
** Description: Burst write values with a length to an address
** Created....: 11.02.2020 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void writeBurstRegister(uint8_t adr, uint8_t* data, uint8_t len)
{
    uint8_t i = 0;

    for(i=0;i<len;i++)
    {
        writeRegister(adr, data[i]);
    }

}

/*==============================================================================
** Function...: setModeRx
** Return.....: void
** Description: Set sx1278 in RX mode
** Created....: 10.02.2020 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void setModeRx(void)
{
    //set tx mode
    uint8_t value = readRegister(RF95_REG_01_OP_MODE);
    value = (value & ~0x07) | (RF95_MODE_RXCONTINUOUS & 0x07);
    writeRegister(RF95_REG_01_OP_MODE, value);
    
}

/*==============================================================================
** Function...: setModeTx
** Return.....: void
** Description: Set sx1278 in TX mode
** Created....: 10.02.2020 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void setModeTx(void)
{
    //set tx mode
    uint8_t value = readRegister(RF95_REG_01_OP_MODE);
    value = (value & ~0x07) | (RF95_MODE_TX & 0x07);
    writeRegister(RF95_REG_01_OP_MODE, value);
    
}

/*==============================================================================
** Function...: setModeIdle
** Return.....: void
** Description: Set sx1278 in idlemode
** Created....: 10.02.2020 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void setModeIdle(void)
{
    uint8_t value = readRegister(RF95_REG_01_OP_MODE);
    value = (value & ~0x07) | (RF95_MODE_STDBY & 0x07);
    writeRegister(RF95_REG_01_OP_MODE, value);
}

/*==============================================================================
** Function...: setModeSleep
** Return.....: void
** Description: Set sx1278 in sleep mode
** Created....: 10.02.2020 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void setModeSleep(void)
{
    uint8_t value = readRegister(RF95_REG_01_OP_MODE);
    value = (value & ~0x07) | (RF95_MODE_SLEEP & 0x07);
    writeRegister(RF95_REG_01_OP_MODE, value);
}

/*==============================================================================
** Function...: setModeLoRa
** Return.....: void
** Description: Set sx1278 in LoRa mode
** Created....: 10.02.2020 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void setModeLoRa(void)
{
    uint8_t value = readRegister(RF95_REG_01_OP_MODE);
    value = (value & ~0x80) | (RF95_LONG_RANGE_MODE & 0x80);
    writeRegister(RF95_REG_01_OP_MODE, value);
}