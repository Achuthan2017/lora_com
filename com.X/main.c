/**
  Generated main.c file from MPLAB Code Configurator

  @Company
    Microchip Technology Inc.

  @File Name
    main.c

  @Summary
    This is the generated main.c using PIC24 / dsPIC33 / PIC32MM MCUs.

  @Description
    This source file provides main entry point for system intialization and application code development.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.95-b-SNAPSHOT
        Device            :  PIC24FJ64GB002
    The generated drivers are tested against the following:
        Compiler          :  XC16 v1.36
        MPLAB 	          :  MPLAB X v5.10
*/

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
*/

#include <stdio.h>

#include "sx1278.h"
#include "mcc_generated_files/system.h"
#include "timerdriver.h"
#include "main.h"
#include "usbdriver.h"
#include "ringBuffer.h"


void io_buffer_handler(void);
void task_handler(void);

slme SLME;

/*
                         Main application
 */
int main(void)
{
    // initialize the device
    SYSTEM_Initialize();
    setupLora();

    initiateReceiver();
    
    while (1)
    {
        io_buffer_handler();
        task_handler();
    }

    return 1;
}


void task_handler(void)
{
    if(printAllReg)
    {
        printAllRegisters();
        printAllReg = 0;
    }
    else if (tx_test)
    {
        while(tx_test)
        {
            initiateSender();
            uint8_t dataout[8] = {1,2,3,4,5,6,7,8};
            send(dataout, 8);
            initiateReceiver();
            delay_ms(1000);
        }
    }
}

void io_buffer_handler(void)
{
    
    if (!buffer_empty(&rxBuf))
    {
    /* 
     * ----------------------------------------------------------------------
     * | RSSI | PRSSI | PSIZE | ADD1 | ADD2 | ADD3 | DATASIZE | PCNT | DATA |
     * |--------------------------------------------------------------------|
     * |  0   |   1   |   2   |  3   |  4   |  5   |    6     |   7  |  8+  |  
     * |--------------------------------------------------------------------|
     * |  1   |   1   |   1   |  1   |  1   |  1   |    1     |   1  | 1-36 |
     * |--------------------------------------------------------------------|  
     */ 
            char s[128];
            uint8_t datain[BUFFER_DATA_SIZE];
            int16_t rssi = 0;
            int16_t prssi = 0;
            uint8_t pktLen;
            uint8_t pktSentLen;
            uint8_t pktCnt=0;
            memset(datain, 0, BUFFER_DATA_SIZE);
            
            read_buffer(datain, &rxBuf);
            
            rssi = datain[0]- RSSICORR;
            prssi = datain[1]- RSSICORR;   
            pktSentLen = datain[2]-5;
            pktLen = datain[7];
            pktCnt = datain[6];
                                    
            sprintf(s,"\n\r Packet [%d] Recieved from: %d%d%d\n\r",
                    pktCnt, datain[3], datain[4], datain[5]);
            usb_print(s);
            delay_ms(60);
            sprintf(s,"RSSI %d, Packet RSSI: %d, Packet Len %d (%d)\n\r", 
                    rssi, prssi, pktLen, pktSentLen);
            usb_print(s);
            delay_ms(60);
            
             sprintf(s,"%x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x %x", 
                     datain[8],datain[9],datain[10],datain[11],
                     datain[12],datain[13],datain[14],datain[15],
                     
                     datain[16],datain[17],datain[18],datain[19],
                     datain[20],datain[21],datain[21],datain[23],
                     
                     datain[24],datain[25],datain[26],datain[27],
                     datain[28],datain[29],datain[30],datain[31],
                     
                     datain[32],datain[33],datain[34],datain[35],
                     datain[36],datain[37],datain[38],datain[39]);
            usb_print(s);
    }
    
    if (!buffer_empty(&txBuf))
    {
        uint8_t dataout[BUFFER_DATA_SIZE] = {0};
        read_buffer(dataout, &txBuf);
        initiateSender();
        send(dataout, dataout[0]);
        initiateReceiver();
        usb_print("Done send\n\r");
    }
}

/**
 End of File
*/

