/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    All rights reserved.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: sx1278.h
        Version:   v0.0
        Date:      2019-10-11
    ============================================================================
 */

#ifndef SX1278_H
#define SX1278_H

#include <stdint.h>

void printAllRegisters(void);
uint8_t setupLora(void);
uint8_t send(uint8_t* data, uint8_t len);
uint8_t initiateReceiver(void);
uint8_t initiateSender(void);
uint16_t SPI1_Exchange8bitBuffer(uint8_t *dataTransmitted, uint16_t byteCount, uint8_t *dataReceived);
void radioIrqHandler( void );




#endif /* SX1278_H */