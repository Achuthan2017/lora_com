/*
 * File:   usbdriver.c
 * Author: Achuthan
 *
 * Created on September 22, 2018, 5:59 PM
 */

#include <string.h>

#include "usbdriver.h"
#include "terminal.h"


int usbdriver_init(void) 
{
    return 0;
}


void usbdriver_handler(void)
{
    uint8_t readBuffer[64];
    
    if( USBGetDeviceState() < CONFIGURED_STATE )
    {
        return;
    }

    if( USBIsDeviceSuspended()== true )
    {
        return;
    }

    if( USBUSARTIsTxTrfReady() == true)
    {

        uint8_t numBytesRead;

        numBytesRead = getsUSBUSART(readBuffer, sizeof(readBuffer));
        
        //sprintf(s, "echo %s\0\r\n",readBuffer );
        

        if(numBytesRead > 0)
        {
            terminal( readBuffer, numBytesRead );
            //putUSBUSART(s,5+numBytesRead);
        }
    }

    CDCTxService();
}



void usb_print( void* data )
{
    if(USBUSARTIsTxTrfReady())
    {
        putsUSBUSART( (char *) data);
    }
}